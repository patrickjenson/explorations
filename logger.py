#!/usr/bin/python3

'''A simple logging utility.'''

from doctest import testmod
from enum import IntEnum, unique

class Logger():
	'''A simple logging utility.'''
	
	default_log_format = "[{timestamp}] [{levelname:>8}] {filename}, line {linenumber:>4}: {message}"
	
	@unique
	class Level(IntEnum):
		'''The different possible levels for a Logger.'''
		
		NOTSET   =  0
		DEBUG    = 10
		INFO     = 20
		WARNING  = 30
		ERROR    = 40
		CRITICAL = 50
		ASSERT   = 60
	
	def __init__(
		self,
		/,
		level: Level = Level.WARNING,
		log_format: str = default_log_format,
		date_format: str = '%y-%m-%d %H:%M:%S',
		log_file: str | None = None,
		print_to_console: bool = True
	):
		
		#pylint: disable=consider-using-with(R1732)
		
		self.__output_level = level
		self.__output_format = log_format
		self.__date_format = date_format
		self.__output_location = None
		self.__print_to_console = print_to_console
		
		if log_file is not None:
			self.__output_location = open(log_file, 'a', encoding='utf-8')
	
	def __del__(self):
		if self.__output_location is not None:
			self.__output_location.close()
	
	def debug(self, message: str = ''):
		'''
		Log a message at the DEBUG level.
		
		>>> Logger(Logger.Level.DEBUG, "{levelname}").debug()
		DEBUG
		'''
		
		self.__print_output(self.Level.DEBUG, message)
	
	def info(self, message: str = ''):
		'''
		Log a message at the INFO level.
		
		>>> Logger(Logger.Level.DEBUG, "{levelname}").info()
		INFO
		'''
		
		self.__print_output(self.Level.INFO, message)
	
	def warning(self, message: str = ''):
		'''
		Log a message at the WARNING level.
		
		>>> Logger(Logger.Level.DEBUG, "{levelname}").warning()
		WARNING
		'''
		
		self.__print_output(self.Level.WARNING, message)
	
	def error(self, message: str = ''):
		'''
		Log a message at the ERROR level.
		
		>>> Logger(Logger.Level.DEBUG, "{levelname}").error()
		ERROR
		'''
		
		self.__print_output(self.Level.ERROR, message)
	
	def critical(self, message: str = ''):
		'''
		Log a message at the CRITICAL level.
		
		>>> Logger(Logger.Level.DEBUG, "{levelname}").critical()
		CRITICAL
		'''
		
		self.__print_output(self.Level.CRITICAL, message)
	
	def assertion(self, message: str = '', success_condition = False):
		'''
		Log a message at the ASSERTION level and assert.
		
		>>> mylogger = Logger(Logger.Level.DEBUG)
		>>> mylogger.assertion()
		Traceback (most recent call last):
		...
		AssertionError
		>>> mylogger.assertion(success_condition=True)
		'''
		
		if not isinstance(success_condition, (bool)):
			success_condition = False
		
		if not success_condition:
			self.__print_output(self.Level.ASSERT, message)
			assert success_condition, message
	
	@property
	def level(self):
		'''
		Get the level of this Logger.
		
		>>> Logger(Logger.Level.DEBUG).level._name_
		'DEBUG'
		'''
		
		return self.__output_level
	
	@level.setter
	def level(self, level: Level):
		'''
		Set the level of this Logger.
		
		>>> mylogger = Logger(Logger.Level.DEBUG)
		>>> mylogger.level._name_
		'DEBUG'
		>>> mylogger.level = Logger.Level.ERROR
		>>> mylogger.level._name_
		'ERROR'
		>>> mylogger.level = Logger.Level.NOTSET
		>>> mylogger.level._name_
		'ERROR'
		'''
		
		if level in self.Level and level is not self.Level.NOTSET:
			self.__output_level = level
	
	def __print_output(self, level: Level, message: str):
		'''Format a message and possibly print it.'''
		
		#pylint: disable=protected-access(W0212)
		
		from datetime import datetime
		from inspect import getframeinfo, stack
		
		if level >= self.__output_level > self.Level.NOTSET:
			caller = getframeinfo(stack()[2][0])
			output_args = {
				'timestamp': datetime.now().strftime(self.__date_format),
				'levelnumber': level.value,
				'levelname': level._name_,
				'filepath': caller.filename,
				'filename': caller.filename.split('/')[-1],
				'function': caller.function,
				'linenumber': caller.lineno,
				'message': message
			}
			output_line = self.__output_format.format(**output_args)
			
			if self.__print_to_console:
				print(output_line)
			
			if self.__output_location is not None:
				print(output_line, file=self.__output_location)

if __name__ == '__main__':
	testmod()
