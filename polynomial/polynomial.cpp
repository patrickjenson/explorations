// These functions compute the result of a polynomial expression.
// Given independentVar := y, coeficients = [7, 4, 5, 9]:
//	the expression evaluated would be 7y^3 + 4y^2 + 5y + 9;
//	generated in the ways below in the comments:

#include <cstddef>
#include <iostream>
#include <iterator>
#include <vector>

using std::cout;
using std::endl;
using V_LDouble = std::vector<long double>;

long double compute_polynomial_iterative(long double const independent_var, V_LDouble const& coeficients) {
	// ((((((7) * y) + (4)) * y) + (5)) * y) + 9
	
	if (0 == coeficients.size()) {
		return 0;
	}
	long double tmp = coeficients[0];
	for (size_t i = 1; i < coeficients.size(); i += 1) {
		tmp *= independent_var;
		tmp += coeficients[i];
	}
	return tmp;
}

long double compute_polynomial_recursive(long double const independent_var, V_LDouble const& coeficients) {
	// 9 + (y * ((5) + (y * ((4) + (y * (7))))))
	
	if (0 == coeficients.size()) {
		return 0;
	}
	V_LDouble all_but_last(coeficents.begin(), std::prev(coeficients.end()));
	long double tmp = compute_polynomial_recursive(independent_var, all_but_last);
	return coeficients.back() + (independent_var * tmp);
}

int main(int const argc, char const *const argv[]) {
	long double const ind = 2;
	V_LDouble const coef = {7, 4, 5, 9};
	cout << "iterative: " << compute_polynomial_iterative(ind, coef) << endl;
	cout << "recursive: " << compute_polynomial_recursive(ind, coef) << endl;
	return 0;
}
