// Exploration of a polynomial evaluator.

// These functions compute the result of a polynomial expression.
// Given independentVar := y, coeficients = [7, 4, 5, 9]:
//	the expression evaluated would be 7y^3 + 4y^2 + 5y + 9;
//	generated in the ways below in the comments:

fn compute_polynomial_iterative(independent_var: f64, coeficients: &[f64]) -> f64 {
	// ((((((7) * y) + (4)) * y) + (5)) * y) + 9
	
	if coeficients.is_empty() {
		return 0.0
	}
	let mut tmp = coeficients[0];
	for item in coeficients.iter().skip(1) {
		tmp *= independent_var;
		tmp += item;
	}
	tmp
}

fn compute_polynomial_recursive(independent_var: f64, coeficients: &[f64]) -> f64 {
	// 9 + (y * ((5) + (y * ((4) + (y * (7))))))
	
	if coeficients.is_empty() {
		return 0.0
	}
	let tmp = compute_polynomial_recursive(independent_var, &coeficients[..coeficients.len() - 1]);
	coeficients[coeficients.len() - 1] + (independent_var * tmp)
}

fn main() {
	let ind = 2.0;
	let coef = vec![7.0, 4.0, 5.0, 9.0];
	println!("iterative: {}", compute_polynomial_iterative(ind, &coef));
	println!("recursive: {}", compute_polynomial_recursive(ind, &coef));
}

// vim: set ts=4 sw=0 sts=-1 noet ai sta:
