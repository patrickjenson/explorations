#!/usr/bin/python3

'''Exploration of a polynomial evaluator.'''

# These functions compute the result of a polynomial expression.
# Given independentVar := y, coeficients = [7, 4, 5, 9]:
#	the expression evaluated would be 7y^3 + 4y^2 + 5y + 9;
#	generated in the ways below in the comments:

from sys import exit as sys_exit
from typing import Sequence

def compute_polynomial_iterative(independent_var: float, coeficients: Sequence[float], /) -> float:
	# ((((((7) * y) + (4)) * y) + (5)) * y) + 9
	
	if len(coeficients) == 0:
		return 0
	tmp = coeficients[0]
	for i in range(1, len(coeficients)):
		tmp *= independent_var
		tmp += coeficients[i]
	return tmp

def compute_polynomial_recursive(independent_var: float, coeficients: Sequence[float], /) -> float:
	# 9 + (y * ((5) + (y * ((4) + (y * (7))))))
	
	if len(coeficients) == 0:
		return 0
	tmp = compute_polynomial_recursive(independent_var, coeficients[:-1])
	return coeficients[-1] + (independent_var * tmp)

if __name__ == '__main__':
	(ind, coef) = (2, [7, 4, 5, 9])
	print(f"iterative: {compute_polynomial_iterative(ind, coef)}")
	print(f"recursive: {compute_polynomial_recursive(ind, coef)}")
	sys_exit(0)
