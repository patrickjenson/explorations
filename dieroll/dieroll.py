#!/usr/bin/python3

'''A simple program to simulate rolling arbitrary dice.'''

#pylint: disable=line-too-long(C0301)

from re import compile as recompile
from random import SystemRandom
from typing import Callable, Sequence

named_group = lambda name: fr"(?P<{name}>[1-9][0-9]*)"
operator_group = fr"(?:(?P<operator>[+\-*/]){named_group('shift')})"
dice_group_pattern = fr"^{named_group('number')}d(?P<sides>(?:[1-9][0-9]*|%))(?P<advantage>a)?{operator_group}?$"
die_unit = recompile(dice_group_pattern)

RAND_RANGE = SystemRandom().randrange

def make_modifier(operator: str | None, shift: str | None, /) -> Callable[[int], int]:
	'''Return a function to modify each die in a set.'''
	
	func = lambda x: x
	if operator is not None and shift is not None:
		ishift = int(str(shift))
		
		if operator == '+':
			func = lambda x: x + ishift
		elif operator == '-':
			func = lambda x: x - ishift
		elif operator == '*':
			func = lambda x: x * ishift
		elif operator == '/':
			func = lambda x: x // ishift
	return func

def calculate_dice(dice_list: Sequence[str], /) -> list[tuple[str, str | int]]:
	'''Generate ouput for given dice.'''
	
	results = []
	
	for die in dice_list:
		die_info = None
		match = die_unit.fullmatch(die)
		if match is not None:
			die_info = (
				int(match['number']),
				match['sides'],
				match['advantage'],
				make_modifier(match['operator'], match['shift'])
			) #yapf:disable
		
		result: str | int = 'Unknown dice'
		if die_info is not None:
			(start, stop, step) = (0, 100, 10) # d% die
			if die_info[1] != '%':
				(start, stop, step) = (1, int(die_info[1]) + 1, 1)
			oper = sum if die_info[2] is None else max
			rolled_dice = [RAND_RANGE(start, stop, step) for _ in range(die_info[0])]
			result = die_info[3](oper(rolled_dice)) #type:ignore
		results.append((die, result))
	
	return results

def print_results(results: list[tuple[str, str | int]], /) -> None:
	'''Display the reults of the dice toss.'''
	
	longest = len(max([die[0] for die in results], key=len))
	for (dice, dset) in results:
		print(f"\n    {dice:>{longest}} = {dset}")
	print('')

def main() -> None:
	'''Main function of die roller.'''
	
	from copy import deepcopy
	from os.path import basename
	from signal import SIGINT, SIGTERM, raise_signal, signal
	from sys import argv, exit as sys_exit
	
	signal(SIGINT, lambda signum, frame: sys_exit(0))
	signal(SIGTERM, lambda signum, frame: sys_exit(0))
	
	if len(argv) > 1:
		if argv[1] == '-h':
			print(f"\nusage: {basename(argv[0])} [-h] [NdS[A][<OP>M] [ DG[ ...]]]\nwhere:")
			print('\tN is a number of dice to roll')
			print('\tS is the number of sides on each of those dice')
			print('\tA is a literal \'a\' to indicate rolling with advantage')
			print('\tOP is an optional modifier for each group; one of {+-*/}')
			print('\tM is the value for the modifier')
			print('\tDG is another dice group like above')
			print('\nOmit all dice to enter a rolling shell.')
			print('A blank line will re-run the previous line.')
			print('^C will exit the rolling shell.')
		
		else:
			print_results(calculate_dice(argv[1:]))
		
		raise_signal(SIGTERM)
	
	else:
		prev_dice: list[str] = []
		while True:
			dice = [ind.strip() for ind in input('\nd> ').split()]
			if len(dice) > 0:
				prev_dice = deepcopy(dice)
				print_results(calculate_dice(dice))
			elif len(prev_dice) > 0:
				#~TODO: Put this on the same line as the prompt; like it had been typed.
				# print(' '.join(prev_dice), end='')
				print(':  ' + ' '.join(prev_dice))
				print_results(calculate_dice(prev_dice))

if __name__ == '__main__':
	main()
