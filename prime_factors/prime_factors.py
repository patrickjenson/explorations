#!/usr/bin/env python3

'''An exploration into prime numbers.'''

from doctest import testmod
from sys import argv, exit as sys_exit
from typing import Generator, Sequence

def make_primes(limit: int, /) -> Generator[int, None, None]:
	'''
	Generate prime numbers <= sqrt(limit).
	
	>>> [*make_primes(529)]
	[2, 3, 5, 7, 11, 13, 17, 19, 23]
	'''
	
	from math import ceil, sqrt
	
	if limit < 4:
		return
	
	yield 2
	
	if limit < 9:
		return
	
	yield 3
	
	# a candidate number c must be such that 2 <= c <= sqrt(n)
	for candidate in range(5, ceil(sqrt(limit)) + 1, 2):
		# if n % 6 in (0, 2, 4): n is divisible by 2 and thus not prime
		# if n % 6 in (0, 3): n is divisible by 3 and thus not prime
		# both of the above are accounted for by putting 2 and 3 before this loop
		if candidate % 6 in (1, 5):
			yield candidate

def format_factors(value: int, factors: dict[int, int], /) -> str:
	'''Format a number and its prime factors in expression form.'''
	
	output = f"    {value} "
	if len(factors) == 1 and value in factors:
		return output + 'is prime'
	if len(factors) == 0: #1
		return output + 'is NOT prime'
	
	output += '= '
	for (prime, count) in factors.items():
		exponent = f"^{count}" if count > 1 else ''
		output += f"{prime}{exponent} * "
	return output[:-3]

def main(arguments: Sequence[str], /) -> int:
	'''Main function.'''
	
	try:
		input_value = int(arguments[1])
	
	except (IndexError, ValueError):
		#an interesting composite number
		input_value = 510510
	
	if input_value < 0:
		print('    Positive number please.')
		sys_exit(1)
	
	working_value = input_value
	factors: dict[int, int] = {}
	
	for prime in make_primes(working_value):
		if working_value == 1:
			break
		while working_value % prime == 0:
			factors[prime] = factors.get(prime, 0) + 1
			working_value //= prime
	
	#the input itself is prime
	if working_value > 1:
		factors[working_value] = 1
	
	print(format_factors(input_value, factors))
	return 0

if __name__ == '__main__':
	CODE = 0
	if len(argv) > 1 and argv[1] in ('-t', '--test'):
		testmod()
	else:
		CODE = main(argv)
	sys_exit(CODE)

# vim: set ts=4 sw=0 sts=-1 noet ai sta:
