#include <cstdarg>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
#include <vector>
#include <dlfcn.h>
#include <unistd.h>

using std::string;

#define TRACELINE fprintf(stderr, "%d\n", __LINE__);

typedef void(*void_func)(...);
static string output_buffer;
static string env_var_name("EXECLOG");

static string build_str_from_list(char *__const args[]) {
	string result;
	if (nullptr != args[0]) {
		result += string("'") + args[0] + "'";
		for (auto ptr = &args[1]; nullptr != ptr && nullptr != *ptr; ++ptr) {
			result += string(", '") + *ptr + "'";
		}
	}
	return result;
}

static void append_entry(
	__const char *__const header,
	__const char *__const func_name,
	__const char *__const executable,
	__const string args,
	__const string env
) {
	char *buffer;
	asprintf(&buffer, header, func_name, executable, args.c_str(), env.c_str());
	output_buffer += buffer;
	free(buffer);
}

static void do_output() {
	char const *output_env = getenv(env_var_name.c_str());
	FILE *tmp = (nullptr != output_env) ? fopen(output_env, "a") : stderr;
	fwrite(output_buffer.c_str(), sizeof(char), output_buffer.size(), tmp);
	(tmp == stderr) ? fflush(tmp) : fclose(tmp);
}

#define TRANSFER_CALL(func) \
	do_output(); \
	typedef typeof(&func) fp_t; \
	static fp_t fp = (fp_t)dlsym(RTLD_NEXT, #func); \
	if (nullptr == fp) { fprintf(stderr, "%s\n", dlerror()); } \
	void *args = __builtin_apply_args(); \
	int *ret = (int *)__builtin_apply((void_func)fp, args, 4096); \
	__builtin_return(ret);

#define AAA(fname) \
	va_list va_args; \
	va_start(va_args, __arg); \
	std::vector<char *> arg_buffer; \
	char *arg = va_arg(va_args, char *); \
	while (nullptr != arg) { \
		arg_buffer.push_back(arg); \
		arg = va_arg(va_args, char *); \
	} \
	arg_buffer.push_back(nullptr); \
	append_entry("%7s:('%s', '%s', {%S})\n", #fname, __path, __arg, build_str_from_list(&arg_buffer[0])); \
	va_end(va_args); \
	TRANSFER_CALL(fname)

int execl (__const char *__path, __const char *__arg, ...) { AAA(execl)  }
int execle(__const char *__path, __const char *__arg, ...) { AAA(execle) }
int execlp(__const char *__path, __const char *__arg, ...) { AAA(execlp) }

#define BBB(fname) \
	append_entry("%7s:('%s', {%s})\n", #fname, __path, build_str_from_list(__argv), ""); \
	TRANSFER_CALL(fname)

int execv (__const char *__path, char *__const __argv[]) { BBB(execv)  }
int execvp(__const char *__path, char *__const __argv[]) { BBB(execvp) }

#define CCC(fname) \
	append_entry("%7s:('%s', {%s}, {%s})\n", #fname, __path, build_str_from_list(__argv), build_str_from_list(__envp)); \
	TRANSFER_CALL(fname)

int execve (__const char *__path, char *__const __argv[], char *__const __envp[]) { CCC(execve)  }
int execvpe(__const char *__path, char *__const __argv[], char *__const __envp[]) { CCC(execvpe) }

int fexecve(int __fd, char *__const __argv[], char *__const __envp[]) {
	append_entry(
		"%7s:(fd: %d, {%s}, {%s})\n",
		__FUNCTION__,
		std::to_string(__fd).c_str(),
		build_str_from_list(__argv),
		build_str_from_list(__envp)
	);
	TRANSFER_CALL(__FUNCTION__)
}

int main([[gnu::unused]] int const argc, char const *const argv[]) {
	string usage_help("LOG_FILE_PATH will send the output to a text file instead of stderr.");
	string usage_str("usage: [%s=<LOG_FILE_PATH>] LD_PRELOAD=%s <PROGRAM_WITH_ARGS>\n\t%s\n");
	printf(usage_str.c_str(), env_var_name.c_str(), argv[0], usage_help.c_str());
	return 0;
}
