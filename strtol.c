#include<errno.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

int main(){
	long i = 3;
	char *input = strdup("tree");
	char *endptr;
	errno = 0;
	i = strtol(input, &endptr, 10);
	if ('\0' != *endptr) {
		printf("%ld: %s\n", i, strerror(errno));
		printf("%p: %p: %d\n", input, endptr, endptr == &input);
	}
	free(input);
	return 0;
}
