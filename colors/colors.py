#!/usr/bin/python3

'''Exploration of RGB and HSB(V) colors.'''

from __future__ import annotations

class Color():
	'''A basic color respresentation, primarily in RGB form.'''
	
	__slots__ = ('red', 'green', 'blue')
	
	RGB_MAX = 0xff
	_PRECISION = 3
	
	@staticmethod
	def from_hex(hex_val: int | str, /) -> Color:
		'''Create a Color from a hex value.'''
		
		if isinstance(hex_val, (str)):
			from re import I, match
			
			if (result := match('^0x([0-9a-f]{6})$', hex_val, I)) is None:
				raise ValueError('Invalid hex color code.')
			hex_val = int(result.group(0), 16)
		
		new_vals = ((hex_val & 0xff0000) >> 16, (hex_val & 0xff00) >> 8, hex_val & 0xff)
		return Color(*new_vals)
	
	def __init__(self: Color, red: int = 0, green: int = 0, blue: int = 0, /) -> None:
		'''Constructor for a color.'''
		super().__init__()
		fraction = lambda x: abs(x) / float(self.RGB_MAX)
		self.red = fraction(red)
		self.green = fraction(green)
		self.blue = fraction(blue)
	
	#{{{ Value
	@property
	def _value(self: Color, /) -> float:
		'''The value of this color (unrounded).'''
		
		return max(self.red, self.green, self.blue)
	
	@property
	def value(self: Color, /) -> float:
		'''The value of this color (rounded).'''
		
		return round(self._value, self._PRECISION)
	#}}}
	
	#{{{ Chroma
	@property
	def _chroma(self: Color, /) -> float:
		'''The chroma of this color (unrounded).'''
		
		return self._value - min(self.red, self.green, self.blue)
	
	@property
	def chroma(self: Color, /) -> float:
		'''The chroma of this color (rounded).'''
		
		return round(self._chroma, self._PRECISION)
	#}}}
	
	#{{{ Saturation
	@property
	def _saturation(self: Color, /) -> float:
		'''The saturation of this color (unrounded).'''
		
		ret = 0.0
		if (val := self.value) != 0:
			ret = self.chroma / val
		return ret
	
	@property
	def saturation(self: Color, /) -> float:
		'''The saturation of this color (rounded).'''
		
		return round(self._saturation, self._PRECISION)
	#}}}
	
	#{{{ Hue
	@property
	def _hue(self: Color, /) -> float:
		'''The hue of this color (unrounded).'''
		
		val = self._value
		if (chroma := self._chroma) == 0:
			hue = 0.0
		elif val == self.red:
			hue = ((self.green - self.blue) / chroma) % 6
		elif val == self.green:
			hue = ((self.blue - self.red) / chroma) + 2
		elif val == self.blue:
			hue = ((self.red - self.green) / chroma) + 4
		return hue * 60
	
	@property
	def hue(self: Color, /) -> float:
		'''The hue of this color (rounded).'''
		
		return round(self._hue, self._PRECISION)
	#}}}
	
	#{{{ Operators
	def __add__(self: Color, other: Color, /) -> Color:
		'''Create a new Color that is the sum of the two Colors.'''
		
		limit = lambda val: round(max(0, min(val * self.RGB_MAX, self.RGB_MAX)))
		new_values = (self.red + other.red, self.green + other.green, self.blue + other.blue)
		return Color(*map(limit, new_values))
	
	def __sub__(self: Color, other: Color, /) -> Color:
		'''Create a new Color that is the sum of the two Colors.'''
		
		limit = lambda val: round(max(0, min(val * self.RGB_MAX, self.RGB_MAX)))
		new_values = (self.red - other.red, self.green - other.green, self.blue - other.blue)
		return Color(*map(limit, new_values))
	
	def __repr__(self: Color, /) -> str:
		'''Present this Color in precise string format.'''
		
		return f"Color:{{red: {self.red}, green: {self.green}, blue: {self.blue}}}"
	
	def __str__(self: Color, /) -> str:
		'''Present this Color in readable string format.'''
		
		#pylint: disable=consider-using-f-string
		
		fix = lambda val: round(val * self.RGB_MAX)
		vals = (self.red, self.green, self.blue)
		return "Color:{{red: {0}, green: {1}, blue: {2}}}".format(*map(fix, vals))
	#}}}
	
	def to_hsv_raw(self: Color, /) -> tuple[float, float, float]:
		'''This color in HSV form (unrounded).'''
		
		return (self._hue, self._saturation, self._value)
	
	def to_hsv(self: Color, /) -> tuple[float, float, float]:
		'''This color in HSV form (rounded).'''
		
		return (self.hue, self.saturation, self.value)
	
	def to_hex(self: Color, /, web_style: bool = False) -> str:
		'''Get a hex code of this color for program use.'''
		
		#pylint: disable=consider-using-f-string
		
		prefix = '#' if web_style else '0x'
		fix = lambda val: round(val * self.RGB_MAX)
		form = "{0}{1:02x}{2:02x}{3:02x}"
		return form.format(prefix, *map(fix, (self.red, self.green, self.blue)))

def _hsv_rgb_formulas(hue: float, sat: float, value: float, /) -> tuple[float, ...]:
	'''Convert a HSV color to RGB by formulas.'''
	
	k_val = lambda n: (n + hue) % 6
	get_part = lambda n: value - (sat * value * max(0, min(k_val(n), 4 - k_val(n), 1)))
	return tuple(map(float, map(get_part, [5, 3, 1])))

def _hsv_rgb_table(hue: float, sat: float, value: float, /) -> tuple[float, ...]:
	'''Convert a HSV color to RGB by table.'''
	
	rgb = [0.0, 0.0, 0.0]
	
	mid = (chroma := sat * value) * (1 - abs((hue % 2) - 1))
	if mid != 0:
		if 2 <= hue < 3 or 5 <= hue < 6:
			rgb[2] = mid
		elif 0 <= hue < 1 or 3 <= hue < 4:
			rgb[1] = mid
		else:
			rgb[0] = mid
	
	if 3 <= hue <= 5:
		rgb[2] = chroma
	elif 1 <= hue <= 3:
		rgb[1] = chroma
	else:
		rgb[0] = chroma
	
	return tuple(map(lambda val: val + value - chroma, rgb))

def hsv_to_rgb(hue: float, sat: float, value: float, /, use_formulas: bool = False) -> tuple[int, ...]:
	'''Convert a HSV color to RGB by table or formula.'''
	
	hue /= 60.0
	if sat > 1:
		sat /= 100.0
	if value > 1:
		value /= 100.0
	
	method = _hsv_rgb_formulas if use_formulas else _hsv_rgb_table
	return tuple(map(lambda val: round(val * Color.RGB_MAX), method(hue, sat, value)))

def main() -> int:
	'''Main function.'''
	
	test_case = 0x362698
	test_vals = ((test_case & 0xff0000) >> 16, (test_case & 0xff00) >> 8, test_case & 0xff)
	print(test_vals, end='\n\n')
	
	my_color = Color(*test_vals)
	print((hsv := my_color.to_hsv_raw()))
	print(my_color.to_hsv())
	print(hsv_to_rgb(*hsv))
	print(hsv_to_rgb(*hsv, use_formulas=True))
	print(my_color.to_hex(), 'same' if my_color.to_hex() == hex(test_case) else 'not same')
	
	my_color2 = Color.from_hex(hex(test_case))
	print(my_color2.to_hsv())
	print(my_color2)
	print(Color.from_hex(test_case).to_hsv())
	print((my_color2 + Color.from_hex(0x010101)).to_hsv())
	
	my_color3 = my_color2 - Color.from_hex(0x020202)
	print(my_color3.to_hsv())
	print(my_color3.to_hex(True))
	
	return 0

if __name__ == '__main__':
	from sys import exit as sys_exit
	sys_exit(main())

# vim: set ts=4 sw=0 sts=-1 noex ai sta:
