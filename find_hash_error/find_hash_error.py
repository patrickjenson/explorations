#!/usr/bin/python3

'''A utility for finding errors in a file by incremental hashing with bisection search.'''

from argparse import ArgumentParser
from sys import argv, exit as sys_exit
from typing import Callable, Sequence

HASH_WIDTH = 8
lines: list[tuple[bytes, str]] = []

def get_hash(index: int, /, width: int = HASH_WIDTH) -> str:
	'''Get a truncated hash value for a line.'''
	
	return lines[index][1][:width].upper()

def get_output_line(index: int, /) -> str:
	'''Get a formatted line of output.'''
	
	index_size = len(str(len(lines)))
	return f"({index + 1:>{index_size}})  {get_hash(index)}:  {lines[index][0].decode()}"

def print_line(oper: Callable[[int], str], line: int, /) -> None:
	'''Print a single line of output.'''
	
	print(oper(max(0, min(line - 1, len(lines) - 1))))

def verify_line(index: int, /) -> bool:
	'''Get response for if given line is correct.'''
	
	answer = input(f"\n{get_output_line(index)}\nIs this line correct? (y/N) ")
	return answer[0].lower() == 'y' if len(answer) > 0 else False

def bisect() -> int:
	'''Bisection search to locate first error in file.'''
	
	last_line = len(lines) - 1
	if verify_line(last_line):
		return -1
	
	(last_correct, last_incorrect) = (-1, 0)
	if verify_line(last_incorrect):
		(last_correct, last_incorrect) = (last_incorrect, last_line)
	
	while last_correct + 1 != last_incorrect:
		index = last_correct + (last_incorrect - last_correct) // 2
		if verify_line(index):
			last_correct = index
		else:
			last_incorrect = index
	
	return last_incorrect

def make_parser() -> ArgumentParser:
	'''Make argument parser.'''
	
	from argparse import FileType, RawTextHelpFormatter as RTHF
	
	parser_epilog = 'program modes:'
	parser_epilog += '\n    hash -> print just the hash of the file up-to and including line N'
	parser_epilog += '\n    line -> print the standard output for line N'
	parser_epilog += '\n   print -> print the whole file by lines with incremental hashes'
	parser_epilog += '\n  bisect -> use a bisect search to find the first line that has an error'
	parser = ArgumentParser(
		description=globals()['__doc__'],
		epilog=parser_epilog,
		formatter_class=RTHF,
		allow_abbrev=False
	)
	
	mode_options = ('hash', 'line', 'print', 'bisect')
	mode_help = 'program mode; see descriptions below'
	parser.add_argument('mode', choices=mode_options, help=mode_help, metavar='MODE')
	
	input_type = FileType('r', encoding='utf-8')
	input_help = 'the file to search for errors in'
	parser.add_argument('inputFile', type=input_type, help=input_help, metavar='FILE')
	
	line_help = "specific line number to display information for in 'hash' and 'line' modes"
	#argparse.SUPPRESS cannot be used with non-str type.
	parser.add_argument('line_number', nargs='?', type=int, help=line_help, metavar='LINE')
	
	length_help = f"leading characters of hash to display; default={HASH_WIDTH}"
	parser.add_argument(
		'-l',
		'--length',
		default=HASH_WIDTH,
		type=int,
		help=length_help,
		metavar='HASH_LENGTH'
	)
	
	return parser

def main(arguments: Sequence[str], /) -> int:
	'''Main function.'''
	
	#pylint: disable=global-statement
	
	from hashlib import md5
	
	global HASH_WIDTH
	
	args = make_parser().parse_args(arguments)
	HASH_WIDTH = args.length
	
	if args.line_number is not None and args.line_number < 1:
		print('Line numbers must be >= 1.')
		return 1
	
	hasher = md5()
	for line in (l.strip().encode() for l in args.inputFile):
		hasher.update(line)
		lines.append((line, hasher.hexdigest()))
	
	if args.mode == 'hash':
		print_line(get_hash, args.line_number)
	
	elif args.mode == 'line':
		print_line(get_output_line, args.line_number)
	
	elif args.mode == 'print':
		for index in range(len(lines)):
			print(get_output_line(index))
	
	elif args.mode == 'bisect':
		if (error_line := bisect()) >= 0:
			print(f"\nThe first line that has an error is: {error_line + 1}")
			return 1
		
		print('\nThe file is completely correct.')
	
	return 0

if __name__ == '__main__':
	sys_exit(main(argv[1:]))

# vim: set ts=4 sw=0 sts=-1 noet ai sta:
