use std::fs::read_to_string;
use std::io::stdin;

use clap::{Parser, Subcommand};
use md5::compute;

type LineVec = Vec<(String, String)>;

#[derive(Debug, Subcommand)]
enum Mode {
	/// print just the hash of the file up-to and including line N
	Hash {
		/// the file to search for errors in
		file: String,
		
		#[arg(value_name = "LINE")]
		/// specific line number to display information for
		line_number: u64,
	},
	
	/// print the standard output for line N
	Line {
		/// the file to search for errors in
		file: String,
		
		#[arg(value_name = "LINE")]
		/// specific line number to display information for
		line_number: u64,
	},
	
	/// print the whole file by lines with incremental hashes
	Print {
		/// the file to search for errors in
		file: String,
	},
	
	/// use a bisect search to find the first line that has an error
	Bisect {
		/// the file to search for errors in
		file: String,
	},
}

#[derive(Debug, Parser)]
/// A utility for finding errors in a file by incremental hashing with bisection search.
struct Cli {
	#[command(subcommand)]
	/// program mode; see descriptions below
	mode: Mode,
	
	#[arg(short, long, required = false, default_value_t = 8, value_name = "HASH_LENGTH")]
	/// leading characters of hash to display
	length: u64,
}

// Get a truncated hash value for a line.
fn get_hash(lines: &LineVec, index: u64, width: u64) -> String {
	lines[index as usize].1[..width as usize].to_uppercase()
}

// Get a formatted line of output.
fn get_output_line(lines: &LineVec, index: u64, width: u64) -> String {
	let index_size = lines.len().to_string().len();
	let hash = get_hash(lines, index, width);
	format!("({:>index_size$})  {}:  {}", index + 1, hash, lines[index as usize].0)
}

// Print a single line of output.
fn print_line(
	oper: &dyn Fn(&LineVec, u64, u64) -> String,
	lines: &LineVec,
	line_number: u64,
	width: u64,
) {
	println!("{}", oper(lines, (line_number - 1).min(lines.len() as u64 - 1).max(0), width))
}

// Get response for if given line is correct.
fn verify_line(lines: &LineVec, index: u64, width: u64) -> bool {
	println!("\n{}\nIs this line correct? (y/N) ", get_output_line(lines, index, width));
	let mut answer = String::new();
	if let Ok(n) = stdin().read_line(&mut answer) {
		if n > 0 && answer.to_lowercase().starts_with('y') {
			return true
		}
	}
	false
}

// Bisection search to locate first error in file.
fn bisect(lines: &LineVec, width: u64) -> i128 {
	let last_line = lines.len() as u64 - 1;
	if verify_line(lines, last_line, width) {
		return -1
	}
	
	let mut last_correct: i128 = -1;
	let mut last_incorrect: u64 = 0;
	if verify_line(lines, last_incorrect, width) {
		last_correct = last_incorrect as i128;
		last_incorrect = last_line;
	}
	
	while last_correct + 1 != last_incorrect.into() {
		let index = last_correct + (last_incorrect as i128 - last_correct) / 2;
		if verify_line(lines, index as u64, width) {
			last_correct = index;
		}
		else {
			last_incorrect = index as u64;
		}
	}
	
	last_incorrect as i128
}

fn get_lines(path: String) -> LineVec {
	let file = read_to_string(path).unwrap();
	
	let mut tmp = Vec::new();
	let mut index = 0u64;
	for line in file.split_inclusive('\n') {
		let digest = format!("{:x}", compute(file[..=index as usize].to_owned()));
		index += line.len() as u64;
		tmp.push((line[..line.len() - 1].to_owned(), digest));
	}
	tmp
}

fn main() -> Result<(), String> {
	let cli = Cli::parse();
	
	match cli.mode {
		Mode::Hash {
			file,
			line_number,
		} => {
			print_line(&get_hash, &get_lines(file), line_number, cli.length);
			Ok(())
		},
	
		Mode::Line {
			file,
			line_number,
		} => {
			print_line(&get_output_line, &get_lines(file), line_number, cli.length);
			Ok(())
		},
	
		Mode::Print {
			file,
		} => {
			let lines = get_lines(file);
			for index in 0..lines.len() as u64 {
				println!("{}", get_output_line(&lines, index, cli.length));
			}
			Ok(())
		},
	
		Mode::Bisect {
			file,
		} => {
			let error_line = bisect(&get_lines(file), cli.length);
			if error_line >= 0 {
				Err(format!("\nThe first line that has an error is: {}", error_line + 1))
			}
			else {
				println!("\nThe file is completely correct.");
				Ok(())
			}
		},
	}
}

// vim: set ts=4 sw=0 sts=-1 noet ai sta:
