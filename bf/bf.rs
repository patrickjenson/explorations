// An implementation of the BrainF**k language interpreter.

use std::env;
use std::fs::File;
use std::io::{self, Read, Seek, SeekFrom};
use std::ops::{Index, IndexMut};
use std::str;

// Unidirectional auto-resizing tape-style memory model.
#[derive(Debug)]
struct InfiniteLinearMemory {
	tape: Vec<u8>,
}

impl InfiniteLinearMemory {
	fn new() -> Self {
		Self {
			tape: vec![0],
		}
	}
	
	fn verify_size(&mut self, index: usize) {
		if index >= self.tape.len() {
			let additional = index + 1 - self.tape.len();
			for _ in 0..additional {
				self.tape.insert(self.tape.len(), 0);
			}
		}
	}
}

impl Index<usize> for InfiniteLinearMemory {
	type Output = u8;
	
	fn index(&self, index: usize) -> &Self::Output {
		// self.verify_size(index);
		&self.tape[index]
	}
}

impl IndexMut<usize> for InfiniteLinearMemory {
	fn index_mut(&mut self, index: usize) -> &mut Self::Output {
		self.verify_size(index);
		&mut self.tape[index]
	}
}

struct Machine {
	tape: InfiniteLinearMemory,
	data_pointer: usize,
	loop_points: Vec<u64>,
}

impl Machine {
	fn new() -> Self {
		Self {
			tape: InfiniteLinearMemory::new(),
			data_pointer: 0,
			loop_points: Vec::new(),
		}
	}
}

fn main() {
	// See: https://en.wikipedia.org/wiki/Brainfuck
	
	let args: Vec<String> = env::args().collect();
	let mut input_file = File::open(&args[1]).unwrap();
	let mut machine = Machine::new();
	
	let increment_pointer = |m: &mut Machine| {
		m.data_pointer += 1;
	};
	let decrement_pointer = |m: &mut Machine| {
		m.data_pointer -= 1;
	};
	let increment_cell = |m: &mut Machine| {
		m.tape[m.data_pointer] += 1;
	};
	let decrement_cell = |m: &mut Machine| {
		m.tape[m.data_pointer] -= 1;
	};
	
	let print_cell = |m: &mut Machine| {
		print!("{}", str::from_utf8(&[m.tape[m.data_pointer]]).unwrap());
	};
	
	let get_data = |m: &mut Machine| {
		let character = io::stdin().bytes().next().unwrap().unwrap() as char;
		m.tape[m.data_pointer] = character.to_digit(10).unwrap() as u8;
	};
	
	let start_loop = |m: &mut Machine, f: &mut File| {
		if m.tape[m.data_pointer] != 0 {
			m.loop_points.push(f.stream_position().unwrap());
		}
		else {
			if m.loop_points[m.loop_points.len() - 1] == f.stream_position().unwrap() {
				m.loop_points.pop();
			}
			let mut buf = [0; 1];
			loop {
				f.read(&mut buf[..]).unwrap();
				if str::from_utf8(&buf).unwrap() == "]" {
					break
				}
			}
		}
	};
	
	let end_loop = |m: &mut Machine, f: &mut File| {
		if m.tape[m.data_pointer] == 0 {
			m.loop_points.pop();
		}
		else {
			f.seek(SeekFrom::Start(m.loop_points[m.loop_points.len() - 1])).unwrap();
		}
	};
	
	let dump_state = |m: &Machine| {
		println!("Loops: {:?}, DP = {},\nCells: {:?}", m.loop_points, m.data_pointer, m.tape);
	};
	
	let mut char_buf: [u8; 1] = [0];
	while let Ok(n) = input_file.read(&mut char_buf[..]) {
		if n == 0 {
			break
		}
		match str::from_utf8(&char_buf).unwrap() {
			">" => increment_pointer(&mut machine),
			"<" => decrement_pointer(&mut machine),
			"+" => increment_cell(&mut machine),
			"-" => decrement_cell(&mut machine),
			"." => print_cell(&mut machine),
			"," => get_data(&mut machine),
			"[" => start_loop(&mut machine, &mut input_file),
			"]" => end_loop(&mut machine, &mut input_file),
			"|" => dump_state(&mut machine), // This is a debugging command I added to BF.
			_ => {},
		}
	}
	
	println!();
}

// vim: set ts=4 sw=0 sts=-1 noet ai sta:
