#!/bin/zsh

setopt ERR_EXIT
setopt NO_UNSET
setopt PIPE_FAIL

if [[ ${#} -lt 2 ]]; then
	echo -e "\tusage: $(basename ${0}) SCRIPT_NAME"
	return 1
fi

if [[ -z "$(whence shtab)" ]]; then
	echo -e "\tShtab is not available."
	return 1
fi

if [[ ! -e "${env_completion_dir:=${HOME}/.zsh}/_${1}" ]]; then
	mkdir -p "${env_completion_dir}"
	script_dir="$(dirname $(realpath -m ${1}))"
	base_dir="$(basename ${script_dir})"
	PYTHONPATH="${script_dir}" shtab -s zsh --{prefix,prog}\ ${1} "${base_dir}.${1}.make_parser" > "${env_completion_dir}/_${1}"
	return 0
fi
