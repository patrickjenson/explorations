#!/bin/zsh

setopt ERR_EXIT PIPE_FAIL #NO_UNSET

library_name='symlib'
code_name='symdriver'
prog_name="${code_name}.out"

cat <<EOF > ${library_name}.h
typedef void (*vvfunc)();
EOF

cat <<EOF > ${library_name}.c
#include <stdio.h>
void hello(){
	printf("hello\n");
}
EOF

cat <<EOF > ${code_name}.c
#include <dlfcn.h>
#include <stdio.h>

#include "${library_name}.h"

int main(const int argc, char const* argv[]){
	void* libhandle = dlopen("${library_name}.so", RTLD_NOW);
	if (libhandle == NULL){
		printf("Could not find library.\n");
		return -1;
	}

	vvfunc hello = (vvfunc)dlsym(libhandle, "hello");
	if (hello == NULL){
		printf("Could not find function.\n");
	}

	(*hello)(); // or hello();
	dlclose(libhandle);
	return 0;
}
EOF

echo ${1}

debug=''
if [[ ${1} == '-d' || ${2} == '-d' ]]; then
	debug='-g3'
fi

setopt XTRACE
gcc -fPIC -shared ${debug} -o ${library_name}.so ${library_name}.cc
gcc ${debug} -Wl,-R. -o ${prog_name} ${code_name}.c -ldl
./${prog_name}
unsetopt XTRACE
echo ''

if [[ ${1} != "-k" ]]; then
	rm -f ${library_name}.{h,c,so} ${code_name}.{c,out}
fi
