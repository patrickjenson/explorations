use std::convert::AsRef;

use base64::{decode, encode};
use clap::{ArgAction, Parser, ValueEnum};
use lazy_static::lazy_static;

#[derive(Debug, Clone, ValueEnum)]
enum Action {
	Encode,
	Decode,
}

#[derive(Debug, Parser)]
/// A simple program to encode and decode a string in base64.
struct Cli {
	#[arg(value_enum)]
	/// what to do with the input
	action: Action,
	
	/// the value to be converted
	input: String,
	
	#[arg(short, long, action = ArgAction::SetTrue)]
	/// whether or not to use the system library
	system: bool,
}

lazy_static! {
	static ref B64_CHARS: Vec<u8> = {
		let mut tmp = Vec::new();
		for c in ('A'..='Z').chain('a'..='z').chain('0'..='9').chain(['+', '/', '=']) {
			tmp.push(c as u8);
		}
		tmp
	};
}

// My implementation of the b64 encode function.
fn my_encode<T: AsRef<[u8]>>(input_value: T) -> String {
	let mut text_chars = String::new();
	for c in input_value.as_ref() {
		text_chars.push_str(format!("{:08b}", c).as_str());
	}
	
	let orig_len = text_chars.len();
	// input must be padded to a multiple of 24 bits.
	for _ in 0..24 - (orig_len % 24) {
		text_chars.push('0');
	}
	
	let mut new_char: String;
	let mut result = Vec::new();
	for i in (0..text_chars.len()).step_by(6) {
		if i < orig_len {
			new_char = String::from(&text_chars[i..i + 6]);
		}
		else {
			new_char = format!("{:b}", B64_CHARS.len() - 1);
		}
		result.push(B64_CHARS[usize::from_str_radix(&new_char, 2).unwrap()]);
	}
	
	String::from_utf8(result).unwrap()
}

// My implementation of the b64 decode function.
fn my_decode<T: AsRef<[u8]>>(input_value: T) -> String {
	let indices: Vec<u8> = input_value
		.as_ref()
		.iter()
		.map(|&x| B64_CHARS.iter().position(|&y| y == x).unwrap() as u8)
		.collect();
	let mut binary = String::new();
	for item in indices {
		if usize::from(item) == B64_CHARS.len() - 1 {
			binary.push_str("000000");
		}
		else {
			binary.push_str(format!("{:06b}", item).as_str());
		}
	}
	
	let mut char_vals = Vec::new();
	for i in (0..binary.len()).step_by(8) {
		let byte = String::from(&binary[i..i + 8]);
		char_vals.push(u8::from_str_radix(&byte, 2).unwrap());
	}
	char_vals.retain(|&x| x != 0); // Remove the padding character(s).
	
	String::from_utf8(char_vals).unwrap()
}

fn main() {
	let cli = Cli::parse();
	// let cli = Cli::parse_from(["", "encode", "message"]);
	// let cli = Cli::parse_from(["", "decode", "bWVzc2FnZQ=="]);
	
	let output: String;
	
	match cli.action {
		Action::Encode => {
			if cli.system {
				output = encode(cli.input);
			}
			else {
				output = my_encode(cli.input);
			}
		},
		Action::Decode => {
			if cli.system {
				output = String::from_utf8(decode(cli.input).unwrap()).unwrap();
			}
			else {
				output = my_decode(cli.input);
			}
		},
	}
	
	println!("{}", output);
}

// vim: set ts=4 sw=0 sts=-1 noet ai sta:
