#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

/*
 *valgrind -s --leak-check=full --show-leak-kinds=all PROG
 *cppcheck --enable=all --std=c99 --suppress=missingIncludeSystem PROG
 */

pid_t getGrandPid(){
	char *const commandBuffer = (char*)calloc(32, sizeof(char));
	sprintf(commandBuffer, "/usr/bin/ps -p %d -o ppid=", getppid());
	pid_t grandparent = 1;
	FILE *output = popen(commandBuffer, "r");
	fscanf(output, " %d", &grandparent);
	pclose(output);
	free(commandBuffer);
	return grandparent;
}

int main(){
	printf("grandparent: %d\n", getGrandPid());
	return 0;
}
